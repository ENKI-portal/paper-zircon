# Table 3 Solution model: Variance-covariance matrix

```{table} Solution model: Variance-covariance matrix
:name: _table_3

| | $\Delta H_{Na_4ZrSi_2O_8}$ | $\Delta H_{K_4ZrSi_2O_8}$ | $\Delta H_{ZrSiO_4}$ | $\Delta S_{ZrSiO_4}$ | $\Delta V_{ZrSiO_4}$ |
| :----- | :----- | :----- | :----- | :----- | :----- |
| $\Delta H_{Na_4ZrSi_2O_8}$ | 3.03716421e+07 | 2.88194625e+07 | 3.43669687e+07 | 2.80147056e+04 | 1.24200080e+02 |
| $\Delta H_{K_4ZrSi_2O_8}$ | 2.88194625e+07 | 2.88456358e+07 | 3.37646855e+07 | 2.75552018e+04 | 1.36610058e+02 |
| $\Delta H_{ZrSiO_4}$ | 3.43669687e+07 | 3.37646855e+07 | 4.23270965e+07 | 3.40635609e+04 | 1. 76449856e+02 |
| $\Delta S_{ZrSiO_4}$ | 2.80147056e+04 | 2.75552018e+04 | 3.40635609e+04 | 2.76605628e+01 | 1.54646644e-01 |
| $\Delta V_{ZrSiO_4}$ | 1.24200080e+02 | 1.36610058e+02 | 1.76449856e+02 | 1.54646644e-01 | 3.00057280e-03 |
