# Figure 4 Zircon Saturation Curves

```{figure} zircon_sat_rhyolite.pdf 
---
name: _figure_4a
---
Fig. 4a High-silica rhyolite
```
```{figure} zircon_sat_Iceland-rhyolite.pdf
---
name: _figure_4b
---
Fig. 4b Icelandic rhyolite
```
```{figure} zircon_sat_California-rhyolite.pdf
---
name: _figure_4c
---
Fig. 4c California rhyolite
```
```{figure} zircon_sat_dacite.pdf
---
name: _figure_4d
---
Fig. 4d Dacite
```
```{figure} zircon_sat_pantellerite.pdf
---
name: _figure_4e
---
Fig. 4e Pantellerite
```
```{figure} zircon_sat_comendite.pdf
---
name: _figure_4f
---
Fig. 4f Comendite
```
```{figure} zircon_sat_trachyte.pdf
---
name: _figure_4g
---
Fig. 4g Trachyte
```
```{figure} zircon_sat_phonolite.pdf
---
name: _figure_4h
---
Fig. 4h Phonolite
```
```{figure} zircon_sat_madupite.pdf
---
name: _figure_4i
---
Fig. 4i Madupite
```
```{figure} zircon_sat_wyomingite.pdf
---
name: _figure_4j
---
Fig. 4j Wyomingite
```