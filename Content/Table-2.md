# Table 2 Solution model: Parameter estimates

```{table} Solution model: Parameter estimates
:name: _table_2

| Parameter: | Value: | Uncertainty: | Units: |
| :--------- | -----: | -----------: | -----: |
| $\Delta H_{ZrSiO_4}$ | 163044 | 6506 | J |
| $\Delta S_{ZrSiO_4}$ | 69.24 | 5.26 | J/K |
| $\Delta V_{ZrSiO_4}$ | 0.10 | 0.0548 | J/bar |
| $\Delta H_{Na_4ZrSi_2O_8}$ | -267990 | 5511 | J | 
| $\Delta H_{K_4ZrSi_2O_8}$ | -74062 | 5371 | J |
| $\Delta S_{Na_4ZrSi_2O_8}$ | 0 | 5.26 | J/K |
| $\Delta S_{K_4ZrSi_2O_8}$  | 0 | 5.26 | J/K |
| $\Delta V_{Na_4ZrSi_2O_8}$ | 0 | 0.0548 | J/bar |
| $\Delta V_{K_4ZrSi_2O_8}$ | 0 | 0.0548 | J/bar |
