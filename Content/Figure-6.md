# Figure 6 Temperature recovery from zircon saturation data sets 

```{figure} Temperature_recovery.pdf
---
name: _figure_6a
---
Fig. 6a Temperature recovery for the calibration data set. Red symbols denote reported temperatures and Zr concentration (with uncertainties), green symbols the most likely model temperature.  Each green symbol is enclosed by a parallelogram (in yellow) whose vertices are defined by the 5% and 95% quartiles associated with Monte Carlo error propagation of model parameters. The reported and most likely model temperature are connected by a black dashed line for each expetimental datum.  The inset plots reported temperature against model temperature for the average Zr concentration with error brackets at the 5% and 95% quartile.
```
```{figure} WandH_1983_temperature_recovery.pdf
---
name: _figure_6b
---
Fig. 6b Temperature recovery for the Watson and Harrison (1983) data set. See legend for panel "a".
```
```{figure} Boehnke_2013_temperature_recovery.pdf
---
name: _figure_6c
---
Fig. 6c Temperature recovery for the Boehnke et al. (2013) data set. See legend for panel "a".
```
```{figure} Gervasoni_et_al.pdf
---
name: _figure_6d
---
Fig. 6d Temperature predictions for the Gervasoni et al. (2016) data set. Experiments are labeled, indicating Zr concentration. Otherwsie, symbols are plotted as described for panel "a".
```