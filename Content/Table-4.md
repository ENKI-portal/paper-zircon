
# Table 4 Bulk compositions of lavas (wt%)

```{table} Bulk compositions of lavas (wt%)
:name: _table_4

| | High-silica rhyolite | Iceland rhyolite | California rhyolite | dacite | pantellerite | comendite | trachyte | phonolite | madupite | wyomingite |
| :----- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| | [^table_ref_3] | [^table_ref_1] | [^table_ref_1] | [^table_ref_1] | [^table_ref_1],[^table_ref_2] | [^table_ref_1] | [^table_ref_1] | [^table_ref_1] | [^table_ref_4] | [^table_ref_4] |
| SiO<sub>2</sub>  | 77.5   | 74.96 | 75.04 | 63.58 | 69.81 | 73.06 | 57.73 | 54.55 | 43.56 | 50.23 |
| TiO<sub>2</sub>  |  0.08  |  0.23 |  0.07 |  0.64 |  0.45 |  0.23 |  1.18 |  1.60 |  2.31 |  2.27 |
| Al<sub>2</sub>O<sub>3</sub> | 12.5   | 12.55 | 12.29 | 16.67 |  8.59 |  9.76 | 17.05 | 19.00 |  7.85 | 11.22 |
| Fe<sub>2</sub>O<sub>3</sub> |  0.207 |  1.72 |  0.33 |  2.24 |  2.28 |  2.74 |  2.55 |  1.75 |  5.57 |  3.34 |
| Cr<sub>2</sub>O<sub>3</sub> |  -     |  -    |  -    |  -    |  -    |  -    |  -    |  -    |  0.04 |  -   |
| FeO   |  0.473 |  0.71 |  0.71 |  3.00 |  5.76 |  2.70 |  4.35 |  3.78 |  0.85 |  1.84 |
| MnO   |  -     |  0.04 |  0.05 |  0.11 |  0.28 |  0.13 |  0.28 |  0.16 |  0.15 |  0.05 |
| MgO   |  0.03  |  0.02 |  0.04 |  2.12 |  0.10 |  0.10 |  1.11 |  1.76 |  11.03 |  7.09 |
| CaO   |  0.43  |  0.90 |  0.58 |  5.53 |  0.42 |  0.32 |  3.10 |  4.07 |  11.89 |  5.99 |
| Na<sub>2</sub>O  |  3.98  |  4.41 |  4.03 |  3.98 |  6.46 |  5.64 |  6.81 |  9.06 |  0.74 |  1.37 |
| K<sub>2</sub>O   |  4.88  |  3.65 |  4.66 |  1.40 |  4.49 |  4.34 |  4.27 |  3.64 |  7.19 |  9.81 |
| P<sub>2</sub>O<sub>5</sub>  |  -     |  0.04 |  0.01 |  -    |  -    |  0.02 |  0.34 |  0.20 |  1.50 |  1.89 |
| H<sub>2</sub>O   |  5.5   |  5.5  |  5.5  |  3.00 |  2.5  |  2.5  |  1.0  |  1.0  |  1.0  |  2.0  |
| CO<sub>2</sub>   |  0.05  |  0.05 |  0.05 |  0.05 |  0.05 |  0.05 |  0.05 |  0.05 |  0.05 | 0.05 | 
| Zr PPM | 100    |  385  |   -   |  150  | 1800  |   -   |   -   |   -   |    2073 |  2073 |
```

[^table_ref_3]: Gualda et al. (2012 {cite}`gualda2012rhyolite`) 

[^table_ref_1]: Carmichael et al. (1974 {cite}`carmichael1974igneous`)

[^table_ref_2]: Lowenstern and Mahood (1991 {cite}`lowenstern1991new`) 

[^table_ref_4]: Carmichael (1967 {cite}`carmichael1967mineralogy`) Madupite is LH.16 and contains appreciable wadeite, Zr<sub>2</sub>K<sub>4</sub>Si<sub>6</sub>O<sub>18</sub>.  This rock has 0.27 wt% ZrO<sub>2</sub>. The Wyomingite is LH.3. The rock has no Zr analysis but other wyomingites have 0.28 wt% ZrO<sub>2</sub>.  All the analyses are from Table 12 (page 50)
