# Zircon Geothermometry and Zr-mineral saturation in natural magmas using rhyolite-MELTS
```{contents}
:local:
```

## Introduction

Zircon geothermometry is an important tool for estimating pre-eruptive temperatures in magmatic systems (Putirka and Tepley, 2008 {cite}`Putirka_2008`). Existing zircon geothermometers (Watson and Harrison, 1983 {cite}`WATSON1983295`; Boehnike at al., 2013 {cite}`boehnke2013zircon`; Gervasoni et al., 2016 {cite}`gervasoni2016zircon`) are formulated using empirical models calibrated from experimental data on saturation conditions of zircon in magmatic silicate melts. In this paper we formulate an extension of the thermodynamic model for silicate liquids contained in rhyolite-MELTS (Ghiorso and Gualda, 2015 {cite}`ghiorso2015h`; Gualda et al., 2012 {cite}`gualda2012rhyolite`; Ghiorso and Sack, 1995 {cite}`ghiorso1995chemical`) to account for the saturation state of zircon and related Zr-minerals as functions of temperature (T) and pressure (P) for compositions of naturally occuring magmatic liquids. We adopt thermodynamic properties of the zirconium-bearing minerals zircon and badeleyite from Robie et al. (1995 {cite}`robie1995thermodynamic`) and calibrate an internally consistent liquid model using the experimental data of Watson and Harrison (1983 {cite}`WATSON1983295`) and Boehnike at al. (2013 {cite}`boehnke2013zircon`). The resulting model is applied as a zircon geothermobarometer and is utilized to estimate saturation conditions of zircon in phase assemblages forming in silicic magmatic systems.

## Model Formulation

Ghiorso and Sack (1995 {cite}`ghiorso1995chemical`) posited a functional form for the Gibbs Free Energy of magmatic composition silicate liquids that was largely based on the model of Ghiorso et al. (1983 {cite}`ghiorso1983gibbs`). Their model assumes that the thermodynamic properties of naturally occuring silicate liquids can be approximated using simple multi-component regular solution theory on the predicate that thermodynamic components are appropriately chosen to represent "mineral-like" stoichiometric compounds. They calibrated this model using experimental phase equilibrium data. The silicate liquid model of Ghiorso and Sack (1995 {cite}`ghiorso1995chemical`; hereafter, MELTS) was not altered in the development of the rhyolite-MELTS model (Gualda et al., 2012 {cite}`gualda2012rhyolite`) that extends MELTS to highly silicic magmatic compositions. The addition of oxidized carbon to the liquid model by Ghiorso and Gualda (2015 {cite}`ghiorso2015h`) did require modification to the underlying thermodynamic formalism for the liquid phase. A thermodynamic component (CO<sub>2</sub>) and a dependent species (CaCO<sub>3</sub>) were added to account for the presence of both molecular CO<sub>2</sub> and carbonate in silicate melts. The addition of a carbonate species necessitated recasting of the regular solution model into a non-ideal assciated solution (Ghiorso and Gualda, 2015 {cite}`ghiorso2015h`).

The addition of Zr to the liquid model of Ghiorso and Gualda (2015 {cite}`ghiorso2015h`) requires the selection of a Zr-bearing thermodynamic component. We adopt ZrSiO<sub>4</sub>, because we assumed that Zr and Si form a strong association in the melt, and because the thermodynamic properties of the solid phase of equivalent stoichiometry (zircon) are well known (Robie et al., 1995 {cite}`robie1995thermodynamic`). Experimental studies (Watson, 1979 {cite}`watson1979zircon`) and petrologiocal observations (Nicholls and Carmichael, 1969 {cite}`nicholls1969peralkaline`) demonstrate that there is a strong relationship between the alkali-content of a melt and its capacity to hold Zr in solution under conditions of zircon saturation. In a series of elegant experiments on melts of widely varying alkali-contents, Watson (1979 {cite}`watson1979zircon`) established that alkali-zirconate species of stoichiometry four alkalis to one Zr likely form. The formation of these species effectively lowers the mole fraction of ZrSiO<sub>4</sub> in the melt, thereby lowering that component's chemical potential, which has the effect of understaurating the melt in zircon. Watson (1979 {cite}`watson1979zircon`) suggested that species Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> and K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> account for the sequestration of zirconium in alkali-rich magmas. It is interesting to note that the stoichiometry of these species is not reflected in the stoichiomtry of alkali-metal, zirconium-bearing minerals that form in magmatic systems (e.g. wadeite, Zr<sub>2</sub>K<sub>4</sub>Si<sub>6</sub>O<sub>18</sub>, Carmichael, 1967 {cite}`carmichael1967mineralogy`), nor to the supppsition of Linthout (1984 {cite}`linthout1984alkali`) that melt species of zirconium should likely reflect alkali-zirconate mineral stoichiometry.

We extend the associated solution model of Ghiorso and Gualda (2015 {cite}`ghiorso2015h`) to include the species Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> and K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub>. Thermodynamic components of the extended model, additional melt species and component-species transformations are summarized in {numref}`_table_1`. Reactions,  

CO<sub>2</sub> + CaSiO<sub>3</sub> = CaCO<sub>3</sub> + SiO<sub>2</sub>,  

ZrSiO<sub>4</sub> + 2Na<sub>2</sub>SiO<sub>3</sub> = Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> + SiO<sub>2</sub>,  

ZrSiO<sub>4</sub> + 4KAlSiO<sub>4</sub> = K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> + 2Al<sub>2</sub>O<sub>3</sub> + 3SiO<sub>2</sub>,  

represent conditions of homogeneous equilibrium, permitting concentrations of melt species to be calculated by zeroing the Gibbs Free Energy change of all three reactions at specifed T, P and bulk composition.  The last two reactions demonstrate that reduction in the chemical potential (activity) of silica in the melt encourages the transfer of Zr to both alkali-zirconate species.

Using the notation summarized in {numref}`_table_1`, the Gibbs Free Energy of solution may be written:  

```{math}
:label: _eq_1
G = \sum_{i=1}^{s} y_i \mu_i^o + R T \sum_{i=1}^{s} y_i \log \left( \frac{y_i}{y_T} \right) + y_w R T \log \left( \frac{y_w}{y_T} \right) + \left( y_T - y_w \right) R T \log \left( \frac{y_T - y_w}{y_T} \right) \\
+ \sum_{i=1}^{s} \sum_{j=i+1}^{s} W_{i,j} \frac{y_i y_j}{y_T}
```

where $n_i$ denotes moles of the $i^{th}$ component and $y_i$ moles of the $i^{th}$ species.  $n_T$ is defined as $\sum_{i=1}^{17} n_i$ and $y_T$ as $\sum_{i=1}^{20} y_i = n_T - y_{19} + y_{20}$. $\mu_i^o$ denotes the chemical potential of the $i^{th}$ species in the standard state, here taken to be the pure substance at any T and P.  $R$ is the universal gas constant, and $W_{i,j}$ refers to temperatrure and pressure independent regular-solution energetic paramters. The number of moles of H<sub>2</sub>O (either $n_w$ or $y_w$) is treated specially in Eq{eq}`_eq_1` to account for its dissolution in the melt as two hydroxyl species (derivation in Ghiorso et al., 1983 {cite}`ghiorso1983gibbs`). 

Differentiation of Eq {eq}`_eq_1` with respect to $n_{ZrSiO_4}$ results in our model expression for the chemical potential of the ZrSiO<sub>4</sub> endmember component:

```{math}
:label: _eq_2
\mu_{ZrSiO_4} = \mu_{ZrSiO_4}^o + R T \log \left( \frac{y_{ZrSiO_4} \left( y_T - y_w \right)}{y_T^2} \right) + \sum_{i=1}^s W_{i,{ZrSiO_4}} \frac{y_i}{y_T} - \sum_{i=1}^s \sum_{j=i+1}^s W_{i,j} \frac{y_i y_j}{y_T^2}
```

Evaluation of Eq {eq}`_eq_2` requires (1) a functional form and parameterization of $\mu_{ZrSiO_4}^o \left( T,P \right)$, (2) estimated values for $W_{i,j}$, and (3) solution of the three conditions of homogeneous equilibrium,

```{math}
:label: _eq_3
\left( \frac{\partial G}{\partial y_{CaCO_3}} \right)_{n_i} = 0 \\
\left( \frac{\partial G}{\partial y_{Na_4ZrSi_2O_8}} \right)_{n_i} = 0 \\
\left( \frac{\partial G}{\partial y_{K_4ZrSi_2O_8}} \right)_{n_i} = 0
```

to determine equilibrium concentrations of species mole numbers ($y_{CaCO_3}, y_{Na_4ZrSi_2O_8}, y_{K_4ZrSi_2O_8}$) for a given bulk composition ($n_i$).

Eq {eq}`_eq_2` may be utilized to compute the saturation chemical affinity for zircon,

```{math}
:label: _eq_4
\textbf{A}^{zircon} = \mu_{ZrSiO_4}^{liquid} - \mu_{ZrSiO_4}^{o,zircon}
```

or equivalent zirconium-bearing solid phase, with solid-liquid heterogeneous equilibrium (saturation) acheived when $\textbf{A}^{zircon}$ is zero.

## Data Sources for Parameter Calibration

We consider four principal sources of experimental data on zircon saturation  for calibration of the model: (1) the original exploratory study of Watson (1979 {cite}`watson1979zircon`), (2) the followup study that constructed the first comprehensive calibration database by Watson and Harrison (1983 {cite}`WATSON1983295`), (3) the study by Boehnke at al. (2013 {cite}`boehnke2013zircon`) that extended the experimental dataset to 2.5 GPa, and (4) the more recent study of Gervasoni et al. (2016 {cite}`gervasoni2016zircon`) that focused on alkaline and aluminous melts. Of these four studies, the two of Watson and Harrison (1983 {cite}`WATSON1983295`) and Boehnke et al. (2013 {cite}`boehnke2013zircon`) will be used for model parameter calibration. Watson (1979{cite}`watson1979zircon`) focused on understanding alakli-zirconate speciation in melts and to do so utilized synthetic-, compositionally-restricted systems. While his results are valuable in that they illuminate the effect of alakli content on zircon saturation, his liquid compositions lie outside of the applicable compositional domain of the MELTS model and consequently cannot be used to provide quantitative constraints.  The experimental study of Gervasoni et al. (2016 {cite}`gervasoni2016zircon`) extends the compositional range of natural liquids beyond that investigated previously, however, the liquids underwent near 100% iron-loss to the capsules during each experimental run, raising the liklihood that the experiments never achieved equilibrium. Tellingly, the zircon saturation geothermometer calibrated by Gervasoni et al. (2016 {cite}`gervasoni2016zircon`) yields results on meta-aluminous high silica rhyolites dramatically at odds with previous models based on the Watson and Harrison (1983 {cite}`WATSON1983295`) and Boehnke et al. (2013 {cite}`boehnke2013zircon`) datasets. This observation suggests that the three datasets are mutually inconsistent, and we choose to utilize as calibrants the two studies whose results are less problematic.  

## Calibration

### Assumptions

Our model expression for the chemical potential of the ZrSiO<sub>4</sub> component (Eq {eq}`_eq_2`) will be parameterized under the following assumptions:

(1) To be compatible with Ghiorso and Gualda (2015 {cite}`ghiorso2015h`; and consequently MELTS and rhyolite-MELTS) the standard state properties of all non-zirconium bearing liquid components and species will be adopted from Ghiorso and Sack (1995 {cite}`ghiorso1995chemical`) as modified by Ghiorso and Gualda (2015 {cite}`ghiorso2015h`). In addition, all regular solution interaction parameters not involving zirconium-bearing species will be adopted from the same source. By so doing we choose to render our Zr-liquid model calibration internally consistent with rhyolite-MELTS 1.1 (Ghiorso and Gualda, 2015 {cite}`ghiorso2015h`), which permits calculation of mixed H<sub>2</sub>O-CO<sub>2</sub> fluid saturated melt, yet preserves phase equilibrium relations associated with the two-feldspar-quartz, water-saturated ternary minimum.

(2) There are 19 species interaction parameters like $W_{ZrSiO_4, i}$, another 19 like $W_{Na_4ZrSi_2O_8, i}$, and another 19 like $W_{K_4ZrSi_2O_8, i}$. As there are not enough calibration data of sufficient compositional variability to constrain all but a few of them, we will assume that all 57 of these paramaters have values of zero. This assumption is supported by the observation that all naturally occuring magmatic liquids have low concentrations of Zr, making it a trace species of low mole fraction. Consequently, energetic contributions to the Gibbs Free energy via the regular solution terms involving zirconium-bearing species will be minimal; Henrian non-ideality will be accounted solely by the standard state and entropic terms in Eqs {eq}`_eq_1` and {eq}`_eq_2`. 

(3) We will assume that $\mu_{ZrSiO_4}^{o}$ can be parameterized as 

```{math}
:label: _eq_5
\mu_{ZrSiO_4}^{o} = \Delta H_{ZrSiO_4} - T \Delta S_{ZrSiO_4} + \left( P - 1 \right) \Delta V_{ZrSiO_4} + \mu_{ZrSiO_4}^{o,zircon}
```

where $\Delta H_{ZrSiO_4}$, $\Delta S_{ZrSiO_4}$, and $\Delta V_{ZrSiO_4}$ are model parameters that account for the offset of the enthalpy, entropy and volume from the solid in the liquid state. Similarly, we will assume that $\mu_{Na_4ZrSi_2O_8}^{o}$ can be parameterized as 

```{math}
:label: _eq_6
\mu_{Na_4ZrSi_2O_8}^{o} = \Delta H_{Na_4ZrSi_2O_8} - T \Delta S_{Na_4ZrSi_2O_8} + \left( P - 1 \right) \Delta V_{Na_4ZrSi_2O_8} + \mu_{ZrSiO_4}^{o} + 2 \mu_{Na_2SiO_3}^{o} - \mu_{SiO_2}^{o}
```

where $\Delta H_{Na_4ZrSi_2O_8}$, $\Delta S_{Na_4ZrSi_2O_8}$, and $\Delta V_{Na_4ZrSi_2O_8}$ are additional model parameters that account for the non-coplanarity of the standard state free energy of the reciporcal reaction:

ZrSiO<sub>4</sub> + 2Na<sub>2</sub>SiO<sub>3</sub> = Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> + SiO<sub>2</sub>

Similarly, $\mu_{K_4ZrSi_2O_8}^{o}$ can be parameterized as 

```{math}
:label: _eq_7
\mu_{K_4ZrSi_2O_8}^{o} = \Delta H_{K_4ZrSi_2O_8} - T \Delta S_{K_4ZrSi_2O_8} + \left( P - 1 \right) \Delta V_{K_4ZrSi_2O_8} \\
+ \mu_{ZrSiO_4}^{o} + 4 \mu_{KAlSiO_4}^{o} - 2 \mu_{Al_2O_3}^{o} - 3 \mu_{SiO_2}^{o}
```

where $\Delta H_{K_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, and $\Delta V_{K_4ZrSi_2O_8}$ are model parameters that account for the non-coplanarity of the standard state free energy of the reciporcal reaction:  

ZrSiO<sub>4</sub> + 4KAlSiO<sub>4</sub> = K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> + 2Al<sub>2</sub>O<sub>3</sub> + 3SiO<sub>2</sub>

These simplifying assumptions yield a nine-parameter model expression for evaluation of the liquid chemical potential term in Eq {eq}`_eq_4`. For the Zr-bearing solid phases, we adopt thermodynamic properties of zircon and badeleyite from Robie et al. (1995).

### Method

The fitting procedure used to calibrate the model requires an initial guess of parameter values. We evaluate Eq {eq}`_eq_4` for each datum in Watson and Harrison (1983 {cite}`WATSON1983295`) and Boehnike et al. (2013 {cite}`boehnke2013zircon`) using a data reduction workflow documented in the accompanying Jupyter notebook ({doc}`../Notebooks/6-Liquid-MELTS-calib-2`). Using a linear least squares proceedure from the statmodels Python package (Seabold and Perktold, 2010 {cite}`seabold2010statsmodels`), we estimate preliminary values of $\Delta H_{ZrSiO_4}$, $\Delta S_{ZrSiO_4}$, and $\Delta V_{ZrSiO_4}$ that minimize residuals of the chemical affinity and negate any temperature or pressure dependence to those residuals. Next, from the conditions of homogeneous equilibrium (Eq {eq}`_eq_3`) we estimate equilibrium concentrations of melt zirconium-bearing species and choose values of $\Delta H_{Na_4ZrSi_2O_8}$ and $\Delta H_{K_4ZrSi_2O_8}$ so that all species have concentrations within three orders of magnitude of each other. The objective of this exercise is to construct an initial guess speciation model that does not embody a bias towards the dominance of a particular melt species. Initial values of $\Delta S_{Na_4ZrSi_2O_8}$, $\Delta V_{Na_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, and $\Delta V_{K_4ZrSi_2O_8}$ are set to zero.

Parameter refinement is obtained utilizing the trust region non-linear least squares method of the optimize module in the SciPy Python package (Virtanen et al., 2020 {cite}`2020SciPy-NMeth`). On initial refinement, two issues emerged. First, the parameter correlation matrix confirmed our expectation that derived values of $\Delta S_{ZrSiO_4}$, $\Delta S_{Na_4ZrSi_2O_8}$, and $\Delta S_{K_4ZrSi_2O_8}$ are highly correlated; $\Delta V_{ZrSiO_4}$, $\Delta V_{Na_4ZrSi_2O_8}$ and $\Delta V_{K_4ZrSi_2O_8}$ are also highly correlated. This data driven observation allows us to make the simplying assumption that $\Delta S_{Na_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, and $\Delta V_{K_4ZrSi_2O_8}$ are zero, which results in the temperature and pressure dependence of $\mu_{Na_4ZrSi_2O_8}^{o}$ and $\mu_{K_4ZrSi_2O_8}^{o}$ to be modeled by $\Delta S_{ZrSiO_4}$, and $\Delta V_{ZrSiO_4}$, respectively, reducing the parameterization of the model to five unknowns. The second issue that emerged from initial refinement is that the relative abundance of Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> dominated that of K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> by orders of magnitude, and did not reflect the relative abundance of Na and K in the experimental glass composition. This result runs contrary to the observation of Watson (1979 {cite}`WATSON1983295`) who concluded that alkali-zirconate speciation is independent of the identity of the alkali. Further parameter refinement clearly requires a constraint to be adopted that implements the priors observation of Watson (1979 {cite}`WATSON1983295`).

Parameter refinement proceeded by adding an additional residual for each experimental observation of the form:

```{math}
:label: _eq_8
w_p \log \left( \frac{ \frac{2 n_{12}}{n_{13}} }{ \frac{y_{19}}{y_{20}} } \right)
```

which corresponds to a weighted ($w_p$) Bayesian logistics function that forces analytical vales of Na/K ($\frac{2 n_{12}}{n_{13}}$) to reflect model estimates of (Na,K)-zirconate species abundance ($\frac{y_{19}}{y_{20}}$). The weighting is chosen to make the logistic residual the same oder of magnitude as the affinity residual, ~2500 J.

Calibration results in the parameter values provided in {numref}`_table_2` and the variance-coveriance matrix reported in {numref}`_table_3`. The standard deviation of recovery of zircon affinities is 3756 J/mol.

Quality of fit is evaluated in {numref}`_figure_1`, {numref}`_figure_2` and {numref}`_figure_3`. {numref}`_figure_1` demonstrates that residuals in zircon affinity show no correlation to alakli-content of the liquid, nor to temperature and pressure.  The lack of temperature and pressure dependence of residuals supports our simplifying assumption that $\Delta S_{Na_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, $\Delta S_{K_4ZrSi_2O_8}$, and $\Delta V_{K_4ZrSi_2O_8}$ may be set to zero. The absence of residual correlation to alkali-content implies that alkli-zirconate speciation accounts for the alkali-effect on the "effective-concentration" (i.e., the activity) of ZrSiO<sub>4</sub> in alkali-rich liquids. {numref}`_figure_2` demonstrates recovery of Zr liquid concentration, calculated by adjusting liquid Zr-content in order to zero zircon affinity, plotted against measured values. Estimated Zr concentrations scatter about a 1:1 line plotted against reported concentrations. There is no systematic change in scatter of predicted versus measured value as a function of Zr-concentration over two orders of magnitude. {numref}`_figure_3` illustrates recovery of priors residuals for the regression data set. While priors residuals are small (fall close to the green line) for typical Na/K melt ratios, there is significant deviation at elevated Na/K. The consequences of these deviations may effect recovery of zircon phase relations in phonolitic or pantelleritic composition melts with high Na/K, and this isue will be further examined below.   

## Discussion

The model calibrated above is evaluated in this section by constructing zircon saturation curves for a variety of natural magma types and by examing zircon saturation in conjunction with phase equilibrium calculations using an augmented rhyolite-MELTS model.

### Zircon Saturation Curves

Bulk compositions of a wide variety of silicic magma types are listed in {numref}`_table_4`. This tabulation includes determinations of Zr concentration. Bulk compositions listed in the first four columns of {numref}`_table_4` are likely zircon saturated, whereas magma types listed in columns five through ten are not associated with a zircon solid phase.  The madupite and wyomingite represent extreme compositions and are saturated with the zirconium-bearing mineral wadeite, Zr<sub>2</sub>K<sub>4</sub>Si<sub>6</sub>O<sub>18</sub>, which forms in lieu of zircon in these rocks. 

For each magma type listed in {numref}`_table_4` a zircon saturation curve is constructed and plotted in {ref}`_figure_4a` through {ref}`_figure_4j`. For each sub-figure, the mean zircon saturation curve is plotted along with its 5% and 95% quartile. Calculations are based on a Monte Carlo propoagtion of uncertainties using parameter estimates constructed from the variance-covariance matrix of {numref}`_table_3`. The right-most panels display abundances of zirconium-bearing species in the melt with equivalent quartile estimates.

At reported Zr concentrations, saturation conditions for fluid-saturated high-silica rhyolite (Bishop Tuff, {ref}`_figure_4a`) are similar to those estimated using the empirical geothermometer of Watson and Harrison (1983).  Saturation conditions for the Iceland rhyolite (Thingmuli, {ref}`_figure_4b`) are consistent with expected higher-temperatures of these lavas given their lower water contents. The Iceland rhyolite also has a higher Na/K ratio than its continental equivalent. The northern California rhyolite displayed in {ref}`_figure_4c` has saturation conditions similar to the Bishop magma, and the dacite ({ref}`_figure_4d`) is undersaturated with zircon at the expected storage temperature conditions, but Zr contents are consistent with saturation for a rhyolitic residuum of this bulk composition.

Despite their high Zr-concentrations, peralkaline rhyolites in {numref}`_table_4` (a pantellerite from Pantelleria and a comendite from Mayor Island, NZ) are not saturated with zircon. Water contents for the pantellerite (Lowenstern and Mahood, 1991) are ~1.8 wt% and consequently the inferred solidus temperatures are high, perhaps comparable to Icelandic rhyolites. Reported Zr abundances in these lavas (1800 and 2300 PPM, respectively) suggest model zircon saturation temperature below ~750°C ({ref}`_figure_4e`) and ~870°C ({ref}`_figure_4f`). Arguing from the basis of phase relations for the ferromagesian silicates and the absence of Fe-Ti oxides associated with these lavas, Nicholls and Carmichael (1969 {cite}`nicholls1969peralkaline`) conclude that likely liquidus temperatures fall between 900°C and 1025°C. Given that water contents of the magma indicate strongly undersaturated conditions, the solidus is likely to be within 100°C of liquidus. Our model predictions of zircon saturation are consistent with these conclusions, indicating that  Zr-concentration is barely sufficient to saturate these lavas with zircon prior to eruption.

Model stauration curves for trachyte ({ref}`_figure_4g`) and phonolite ({ref}`_figure_4h`) reveal that Zr-concentrations must be on the order of a weight percent in order to saturate these liquids with zircon. An additional observation is that the shape of the saturation curves is strongly influenced by  temperature. To understand why, consider that these liquids simultaneously have higher absolute concentrations Zr and Na and higher Na/K ratios; the Na-zirconate species has abundance comparable to ZrSiO<sub>4</sub>.  Assuming $x$ is the fraction of Na-zirconate species and $1-x$ is the fraction of ZrSiO<sub>4</sub>, the dissolution of zircon proceeds via the reaction:

ZrSiO<sub>4</sub><sup>*zircon*</sup> + $2 x$ Na<sub>2</sub>SiO<sub>3</sub><sup>*liquid*</sup> = $x$ Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub><sup>*liquid*</sup> + $\left( 1-x \right)$ ZrSiO<sub>4</sub><sup>*liquid*</sup> + $x$ SiO<sub>2</sub><sup>*liquid*</sup>

The temperature dependence of zircon saturation is consequently a reflection of the entropy of this dissolution reaction, which is 

```{math}
:label: _eq_9
\Delta S = x \bar{s}_{Na_4ZrSi_2O_8}^{liquid} + \left( 1-x \right) \bar{s}_{ZrSiO_4}^{liquid} + x \bar{s}_{SiO_2}^{liquid} - 2 x \bar{s}_{Na_2SiO_3}^{liquid} - \bar{s}_{ZrSiO_4}^{zircon}
```

The standard state entropy change contribution to Eq {eq}`_eq_9` is zero under the model calibration asumptions made previously, so, this expression reduces to:

```{math}
:label: _eq_10
\Delta S &\approx - x R \log X_{Na_4ZrSi_2O_8} - \left( 1-x \right) R \log X_{ZrSiO_4} - x R \log X_{SiO_2} + 2 x R \log X_{Na_2SiO_3} \\
&\approx - R \log \left( \frac{X_{Na_4ZrSi_2O_8}^x X_{ZrSiO_4}^{1-x} X_{SiO_2}^x}{X_{Na_2SiO_3}^{2x}} \right) = - R \log \left( X_{Na_4ZrSi_2O_8}^x X_{ZrSiO_4}^{1-x} \right) - R x \log \left( \frac{X_{SiO_2}}{X_{Na_2SiO_3}^{2}} \right)
```

if we consider non-ideal contributions to the Gibbs Free Energy to be second order. If $z$ is the total mole fraction of all zirconate species, then $X_{Na_4ZrSi_2O_8}$ is approximately $z x$ and $X_{ZrSiO_4}$ is approcimately $z \left( 1-x \right)$, and Eq {eq}`_eq_10` becomes

```{math}
:label: _eq_11
\Delta S &\approx - R \log \left[ z^x x^x z^{1-x} \left( 1 - x \right) ^{1-x} \right] - R x \log \left( \frac{X_{SiO_2}}{X_{Na_2SiO_3}^{2}} \right) \\
&= -R \log z - R \left[ x^x \left( 1 - x \right) ^{1-x} \right] - R x \log \left( \frac{X_{SiO_2}}{X_{Na_2SiO_3}^{2}} \right)
```

The last logaithmic term in Eq {eq}`_eq_11` is a linear function of the fraction of Na-zirconate species ($x$) and for the trachyte bulk composition is approximately $- 41 x$ J. The first term has a value of approximately 40 J, but decreases as Zr concentration in the melt increases. The central term has a positive maximum at $x = \frac{1}{2}$ of ~ 6 J and falls parabolically to zero at lower or higher Na-zirconate mole fractions.

This analysis allows us to better understand the shape of the zircon saturation curves for the trachyte and phonolite. A positive slope for these curves requires a positive $\Delta S$, as the dissolution reaction must go to the right as temperature increases, and in previous cases, where the total mole fraction of Zr-bearing species in the liquid is very low, the first term in Eq {eq}`_eq_11` dominates and the saturation curves have a positive slope. However, as the Zr-concentration in the melt increases, the dominance of the first term diminishes, the contribution of the second and third terms become significant in determing the sign of the entropy change. In particular, if the fraction of Na-zirconate species decreaes with increasing *T*, then the third term in Eq {eq}`_eq_11`, which is negative, is more likely to dominate the exression and lead to a turnover in the saturation curve. The conclusion is that higher overall concentration of Zr in the liquid coupled with the presence of near equal abundances of Na-zirconate and ZrSiO<sub>4</sub> species, governs the unusual shape of the zircon saturation curves. If these magmas did saturate with zircon, its presence would lead to an unsatisfactory geothermometer.

The final magma types considered, maduite ({ref}`_figure_4i`) and wyomingite ({ref}`_figure_4j`), represent bulk compositions with extremely high K/Na ratios. Neither magma precipitates zircon, but both form a Zr-bearing phenocryst phase, wadeite. Reported concentrations of Zr are at the 2000 PPM level. Our model calculations suggest that measured concentrations are about an order of magnitude below those required to saturate zircon, and that Zr is predominantly presence as both K-zirconate and ZrSiO<sub>4</sub> species in the melt.

The calculations summarized in {ref}`_figure_4a` through {ref}`_figure_4j` were performed at a pressure of 200 MPa. The effect of pressure on these results is insignificant, e.g. for the high-silica rhyolite assuming 100 PPM Zr in the liquid, the saturation temperature is 744.7°C at 100 Ma, 745.5°C at 200 MPa, 745.9°C at 300 MPa.  This effect is typical for all compositions examined.

### Phase Equilibria

As the calibrated model is compatible with rhyolite-MELTS (v. 1.1), we may use it to calculate phase equilibria in zircon-bearing assemblages. In {numref}`_figure_5` phase proportions during crystallization of a high-silica rhyolite ({numref}`_table_4`) are illustrated. The workflow for this calculation is documented in {doc}`../Notebooks/7-MELTS-with-Zr`.

The most notable feature of the phase diagram presented in {numref}`_figure_5` is that the onset of zircon saturation is at 757.4°C in contrast to the calculated geothermometer temperature of 745.9°C (see below). This temperature difference is due largely to the effect of expulsion of fluid and the crystallization of two feldspars and quartz, which elevates the concentration of the incompatible element Zr in the liquid, permiting saturation of zircon at higher temperature.  This temperature discrepancy highlights the liklihood that application of zircon geothermometry to bulk rock compositions is problematic, even when such compositions are representitive of near eutectic melts. In practical terms, the uncertainty in calculated phase relations is large enough to encompass these temperature differences, but the relative offset is important to consider, and every effort should be made to utilize high quality glass analyses to avoid systematic bias.

### Geothermometry

Software is available to utilize the model presented here as a geothermometer ({doc}`../Notebooks/6-Liquid-MELTS-calib-2`). In {numref}`_figure_6a` through {numref}`_figure_6d` we use this software to calculate apparent experimental temperatures for the calibration data set and for the data set of Gervasoni et al. (2016 {cite}`gervasoni2016zircon`). These results provide an alternative measure of how well the model recovers the calibration data set, and suggests some potential problems with attainment of equilibrium in the data set of Gervasoni.

Temperature estimates for the calibration data set are plotted in {numref}`_figure_6a` with data from Watson and Harrison (1983 {cite}`WATSON1983295`) isolated in {numref}`_figure_6b` and selected low-Zr experiments from Boehnke et al. (2103 {cite}`boehnke2013zircon`) displayed in {numref}`_figure_6c`. Experimental values with uncertainties are plotted in red and temperature estimates with model uncertainties are plotted as yellow polygons, with a green dot at the most likely value. The inset on each panel shows model temperture estimates with associated 5% and 95% quartiles, computed using the average analytical Zr concentration, plotted against reported experimental temperaure.   

Model temperature estimates scatter about experimental temperatures for Zr liquid concentrations below 4000 PPM. Above that concentration, the model tends to overestimate temperature.  This failure is probably related to our model formulation assumption that regular solution interaction parameters involving Zr-species are zero, implicitely rendering the model applicable to a Henrian concentration limit.  This assumption could be relaxed in order to accomodate these data, but there are so few data points at higher Zr-concentrations available for calibration that the danger of overfitting does not warrant this extension.  Model error in temperature estimates due to parameter error propagation is -7.3°C, +8.1°C (5%, 95%) for the Watson and Harrison (1983 {cite}`WATSON1983295`) data set ({numref}`_figure_6b`), with the average error in recovered experimental temperature of -14°C (± 35°C). Including measured uncertainty in Zr concentration brings the average recovered temperature within the range -34°C (Zr - 2 sigma) to 4°C (Zr + 2 sigma) with the standard deviation of the low and high estimates maintained at the same level as the average Zr value.  For the Boehnke et al. (2013 {cite}`boehnke2013zircon`) data set ({numref}`_figure_6c`, ignoring the 4 data points above 4000 PPM Zr), the recovery of temperature estimates due to parameter error propagation is -11.6°C, +10.1°C (5%, 95%). The average error in recovered experimental temperature is -15°C (± 41°C) with the range -17°C ± 35°C (Zr - 2 sigma) to -5°C ± 39°C (Zr + 2 sigma).

Temperature recovery of the data set of Gervasoni et al (2016 {cite}`gervasoni2016zircon`), which was not used in calibration, is shwon in {numref}`_figure_6c`. Note that model temperatures are overestimated for glass compositions with > 6000 PPM Zr, as found with the data set of Boehnke et al. (2013 {cite}`boehnke2013zircon`). At experimental temperatures of 1200°C at at Zr concentrations > 2500 PPM and < 6000 PPM, temperature recovery is comparable to the calibration data sets.  At lower Zr concentrations and at lower temperatures, temperature recovery is systematically biased.  The experimental data set of Gervasoni et al. (2016) differes from the calibration data sets in showing almost complete iron-loss to the experimental capsule. In addition, these experiments were conducted under nominally dry conditions (unlike the other two studies) and run durations were realtively short (24 hrs). Considering the absence of volatiles, which would aid reaction kinetics, all these factors support our contention that equilibrium was not achieved in the lower temperature, lower Zr-content runs. In fact, the partial agreement at 1200°C for the subset of "intermediate" Zr concentration experiments may simply be fortuitous. 

## Model Access 

The model and dependent calculations presented here can be replicated in full utilizing the associated Jupyter notebooks: {doc}`../Notebooks/1-Model-description`), {doc}`../Notebooks/2-Endmembers-MELTS`),{doc}`../Notebooks/3-Liquid-MELTS-codegen`),{doc}`../Notebooks/4-Liquid-MELTS-API-tests`),{doc}`../Notebooks/5-Liquid-MELTS-calib-1`),{doc}`../Notebooks/6-Liquid-MELTS-calib-2`),{doc}`../Notebooks/7-MELTS-with-Zr`).  These notebooks will execute on an ENKI enabled server platform (http://enki-portal.org/index.html) or, more conveniently, within a containerized image implementation of the ThermoEngine software ecosystem (the ENKI software engine) that may be downloaded from GitLab's Docker image containier repository at https://gitlab.com/ENKI-portal/ThermoEngine. The container will execute on any computer that has a recent version of Docker (https://www.docker.com) installed.

## Conclusions

This paper updates MELTS (v 1.1) to account for phase relations involving the trace element Zr. The saturation surface for zircon is calibrated using experimental data from Watson and Harrison (1983 {cite}`WATSON1983295`) and Boehnke et al. (2013 {cite}`boehnke2013zircon`). The model is formulated and optimized for Zr melt concentrations in the effective Henrian limit (< 4000 PPM) and accounts for the effect of alkalis on zircon saturation via the adoption of an associated solution formulation that incorporates three Zr-bearing melt species: ZrSiO<sub>4</sub>, Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub>, and K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub>. The model may be used (1) to calculate zircon phase relations in magmatic composition melts, (2) to construct zircon saturation diagrams for a given liquid bulk composition, and (3) as a geothermometer for zircon bearing igneous rocks.

## Aknowledgements

MSG is grateful to the extended MESSY group at Vanderbilt University for suggesting this problem and for assisting in working through model assumptions and calibration data sets. MSG also acknowledges material support from NSF 17-25425 and from OFM Research.
