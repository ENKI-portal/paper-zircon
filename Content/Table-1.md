# Table 1 Solution model: Components, species and mappings

```{table} Solution model: Components, species and mappings
:name: _table_1

| Components: | Species: | Component: | Species X: |
| :---------- | :------- | :--------- | :--------- |
| SiO<sub>2</sub> | SiO<sub>2</sub> | $n_1$ | $y_1$ = $n_1$ + $y_{18}$ + $y_{19}$ + 3 $y_{20}$ |
| TiO<sub>2</sub> | TiO<sub>2</sub> | $n_2$ | $y_2$ = $n_2$ |
| Al<sub>2</sub>O<sub>3</sub> | Al<sub>2</sub>O<sub>3</sub> | $n_3$ | $y_3$ = $n_3$ + 2 $y_{20}$ |
| Fe<sub>2</sub>O<sub>3</sub> | Fe<sub>2</sub>O<sub>3</sub> | $n_4$ | $y_4$ = $n_4$ |
| MgCr<sub>2</sub>O<sub>4</sub> | MgCr<sub>2</sub>O<sub>4</sub> | $n_5$ | $y_5$ = $n_5$ |
| Fe<sub>2</sub>SiO<sub>4</sub> | Fe<sub>2</sub>SiO<sub>4</sub> | $n_6$ | $y_6$ = $n_6$ |
| MnSi<sub>1/2</sub>O<sub>2</sub> | MnSi<sub>1/2</sub>O<sub>2</sub> | $n_7$ | $y_7$ = $n_7$ |
| Mg<sub>2</sub>SiO<sub>4</sub> | Mg<sub>2</sub>SiO<sub>4</sub> | $n_8$ | $y_8$ = $n_8$ |
| NiSi<sub>1/2</sub>O<sub>2</sub> | NiSi<sub>1/2</sub>O<sub>2</sub>  | $n_9$ | $y_9$ = $n_9$ |
| CoSi<sub>1/2</sub>O<sub>2</sub> | CoSi<sub>1/2</sub>O<sub>2</sub> | $n_{10}$ | $y_{10}$ = $n_{10}$ |
| CaSiO<sub>3</sub> | CaSiO<sub>3</sub> | $n_{11}$ | $y_{11}$ = $n_{11}$ - $y_{18}$ |
| Na<sub>2</sub>SiO<sub>3</sub> | Na<sub>2</sub>SiO<sub>3</sub> | $n_{12}$ | $y_{12}$ = $n_{12}$ - 2 $y_{19}$ |
| KAlSiO<sub>4</sub> | KAlSiO<sub>4</sub> | $n_{13}$ | $y_{13}$ = $n_{13}$ - 4 $y_{20}$ |
| Ca<sub>3</sub>(PO<sub>4</sub>)<sub>2</sub> | Ca<sub>3</sub>(PO<sub>4</sub>)<sub>2</sub> | $n_{14}$ | $y_{14}$ = $n_{14}$ |
| H<sub>2</sub>O | H<sub>2</sub>O | $n_{15}$ | $y_{15}$ = $n_{15}$ |
| CO<sub>2</sub> | CO<sub>2</sub> | $n_{16}$ | $y_{16}$ = $n_{16}$ - $y_{18}$ |
| ZrSiO<sub>4</sub> | ZrSiO<sub>4</sub> | $n_{17}$ | $y_{17}$ = $n_{17}$ - $y_{19}$ - $y_{20}$ |
| | CaCO<sub>3</sub> | | $y_{18}$ |
| | Na<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> | | $y_{19}$ |
| | K<sub>4</sub>ZrSi<sub>2</sub>O<sub>8</sub> | | $y_{20}$ |
```
