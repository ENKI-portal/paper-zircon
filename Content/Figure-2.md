# Figure 2 Zr Content Recovery

```{figure} Figure_Zr_Zr.pdf
---
name: _figure_2
---
Model Reported Zr concentrations (ordinate) plotted against model estimate Zr concentrations (abscissa) for the calibration data set. The estimate is constructed by finding the Zr concentration that zeros the chemical affinity for zircon saturation in the experimental liquid at fixed temperature and pressure.
```