# Figure 3 Na/K Priors

```{figure} Figure_Na_K.pdf
---
name: _figure_3
---
Model predicted Na/K ratios of alkali-zirconate species in the liquid (abscissa) plotted against measured bulk Na/K ratio in the liquid (ordinate). Green line refers to exact correlation.  This constraint is imposed as a logistic on model calibration. See text.
```