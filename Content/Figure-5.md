# Figure 5 Phase relations for high-silica rhyolite

```{figure} MELTS-zr-rhyolite.pdf
---
name: _figure_5
---
Phase relations for high-silica rhyolite calculated using rhyolite-MELTS (v1.1) and the model presented in this paper. Equilibrium crystallization. Dashed line denotes the onset of zircon saturation at 757.4°C.
```