# Figure 1 Model Residuals

```{figure} Figure_Residuals.pdf
---
name: _figure_1
---
Model Residuals (J) plotted against (upper left) sodium content of the liquid, as moles of Na2SiO3, (upper right) potasssium content of the liquid, as moles of KAlSiO4, (lower left) temperature (K), and (lower right) pressure (bars). Red crosses correspond to residuals in the chemical affinity for the zircon saturation reaction, and blue crosses correspond to residuals in the Na/K logistic function utilized to inforce the prior that the ratio of Na to K zirconate species abundance is strongly correlated to the measured Na/K ratio in the liquid.
```