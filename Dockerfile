FROM registry.gitlab.com/enki-portal/thermoengine:master
COPY . ${HOME}
USER root
RUN pip install -U "jupyter-book>=0.7.0b"
USER ${NB_USER}
